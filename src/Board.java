// Bobby Youstra
// 2dTicTacToeRL

import java.util.ArrayList;

public class Board {

    // Board class: Stores current State and derived States
    // State inner class: Stores a single board state
    // Vector2 inner class: Helper class for position storage (ie row/column)
    public static class State {
        public static class Vector2 {
            private int r;
            private int c;

            public Vector2(int r, int c) {
                this.r = r;
                this.c = c;
            }

            public int getColumn() {
                return c;
            }

            public int getRow() {
                return r;
            }

            @Override
            public String toString() {
                return r + "," + c;
            }
        }

        Boolean[][] choices;
        private Integer[][] marks;
        int degreesRelation = 0; // used for pairing original states with
                                 // derivative states (in degrees)
        private int moveNum = 0;

        public Boolean[][] copyBoolMatrix(Boolean[][] matrix) {
            Boolean[][] returnValue = new Boolean[matrix.length][matrix[0].length];

            for(int r = 0; matrix.length > r; ++r) {
                for(int c = 0; matrix[0].length > c; ++c) {
                    returnValue[r][c] = matrix[r][c].booleanValue();
                }
            }

            return returnValue;
        }

        public Integer[][] copyIntegerMatrix(Integer[][] matrix) {
            Integer[][] returnValue = new Integer[matrix.length][matrix[0].length];

            for(int r = 0; matrix.length > r; ++r) {
                for(int c = 0; matrix[0].length > c; ++c) {
                    if(matrix[r][c] == null) {
                        returnValue[r][c] = matrix[r][c];
                    } else {
                        returnValue[r][c] = matrix[r][c].intValue();
                    }
                }
            }

            return returnValue;
        }

        State() {
            choices = new Boolean[3][3];

            for(int r = 0; 3 > r; ++r) {
                for(int c = 0; 3 > c; ++c) {
                    choices[r][c] = true;
                }
            }

            marks = new Integer[3][3];
        }

        State(Integer[][] marks, Boolean[][] choices, int degreesRelation, int moveNum) {
            this.choices = choices;
            this.marks = marks;
            this.degreesRelation = degreesRelation;
            this.moveNum = moveNum;
        }

        State(Integer[][] marks, Boolean[][] choices) {
            this(marks, choices, 0, 0);
        }

        public int getMoveNum() {
            return moveNum;
        }

        public boolean isOccupied(int r, int c) {
            return marks[r][c] != null;
        }

        public boolean isOccupied(Vector2 vector2) {
            return isOccupied(vector2.getRow(), vector2.getColumn());
        }

        public ArrayList<Vector2> availableMoves() {
            ArrayList<Vector2> returnValue = new ArrayList<>(9);

            for(int r = 0; 3 > r; ++r) {
                for(int c = 0; 3 > c; ++c) {
                    if(choices[r][c] && !isOccupied(r, c)) {
                        returnValue.add(new Vector2(r, c));
                    }
                }
            }

            return returnValue;
        }

        public int whoWon() {
            // i swear if i forgot a win condition this will be 50
            // times harder to debug
            // returns -1 for no win by anyone,
            // returns 0 for X(AI),
            // returns 1 for O(opponent),
            // returns 2 for ties

            if(marks[0][0] != null && marks[1][1] != null &&
               marks[2][2] != null) {
                if (marks[0][0].equals(marks[1][1]) &&
                        marks[1][1].equals(marks[2][2])) {
                    return marks[0][0];
                }
            }

            if(marks[0][2] != null && marks[1][1] != null &&
               marks[2][0] != null) {
                if (marks[0][2].equals(marks[1][1]) &&
                    marks[1][1].equals(marks[2][0])) {
                    return marks[0][2];
                }
            }

            for(int r = 0; 3 > r; ++r) {
                if(marks[r][0] != null && marks[r][1] != null &&
                   marks[r][2] != null){
                    if(marks[r][0].equals(marks[r][1]) &&
                       marks[r][1].equals(marks[r][2])) {
                        return marks[r][0];
                    }
                }
            }

            for(int c = 0; 3 > c; ++c) {
                if(marks[0][c] != null && marks[1][c] != null &&
                   marks[2][c] != null) {
                    if (marks[0][c].equals(marks[1][c]) &&
                        marks[1][c].equals(marks[2][c])) {
                        return marks[0][c];
                    }
                }
            }

            for(int r = 0; 3 > r; ++r) {
                for(int c = 0; 3 > c; ++c) {
                    if(marks[r][c] == null) {
                        return -1;
                    }
                }
            }

            return 2;
        }

        public Boolean[][] adaptBasedOffDegreesRelation() {
            // uses the degrees rotation variable to adjust the current board's
            // choices array to match the current derivative
            // (or original if the degreesRelation is 0) board.
            assert degreesRelation == 0 || degreesRelation == 90 ||
                   degreesRelation == 180 || degreesRelation == 270;

            Boolean[][] returnValue = copyBoolMatrix(choices);
            Boolean[][] temp = new Boolean[3][3];
            int rotationTimes = degreesRelation / 90;

            for(int i = 0; rotationTimes > i; ++i) {
                temp[0][0] = returnValue[2][0];
                temp[0][1] = returnValue[1][0];
                temp[0][2] = returnValue[0][0];
                temp[1][0] = returnValue[2][1];

                temp[1][1] = returnValue[1][1];

                temp[1][2] = returnValue[0][1];
                temp[2][0] = returnValue[2][2];
                temp[2][1] = returnValue[1][2];
                temp[2][2] = returnValue[0][2];

                returnValue = copyBoolMatrix(temp);
            }

            return returnValue;
        }

        @Override
        public State clone() {
            return new State(copyIntegerMatrix(marks), copyBoolMatrix(choices), degreesRelation, moveNum);
        }

        public static char toSymbol(Integer i) {
            if (i == null) {
                return ' ';
            } else if(i == 0) {
                return 'X';
            } else if (i == 1) {
                return 'O';
            } else {
                return '?';
            }
        }

        public Integer get(int r, int c) {
            return marks[r][c];
        }

        @Override
        public String toString(){
            return " " + toSymbol(marks[0][0]) + " | " + toSymbol(marks[0][1]) + " | " +
                    toSymbol(marks[0][2]) + "\n---+---+---\n " +
                    toSymbol(marks[1][0]) + " | " + toSymbol(marks[1][1]) + " | " +
                    toSymbol(marks[1][2])+ "\n---+---+---\n " +
                    toSymbol(marks[2][0]) + " | " + toSymbol(marks[2][1]) + " | " +
                    toSymbol(marks[2][2]);
        }


        @Override
        public boolean equals(Object o) {
            if(o instanceof State) {
                State state = (State)o;

                return arrayEquals(this.marks, state.marks);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            // hashCode of state based off of sum of each space by:
            // 2 = blank space, 3 = X, 5 = O then multiply by
            // 7 = corner, 11 = edge, 13 = center
            int returnValue = 0;

            for(int r = 0; 3 > r; ++r) {
                for(int c = 0; 3 > c; ++c) {
                    int spaceMultiple;

                    if(isCorner(r, c)) {
                        spaceMultiple = 7;
                    } else if(r == 1 && c == 1) {
                        spaceMultiple = 13;
                    } else {
                        spaceMultiple = 11;
                    }

                    if(marks[r][c] == null) {
                       returnValue += 2 * spaceMultiple;
                    } else if(marks[r][c] == 0) {
                        returnValue += 3 * spaceMultiple;
                    } else if(marks[r][c] == 1) {
                        returnValue += 5 * spaceMultiple;
                    }
                }
            }

            return returnValue;
        }
    }

    ArrayList<State> derivStates;
    State state = new State();

    private State ninetyDegreeRotate(State state) {
        Integer[][] temp = new Integer[3][3];
        Boolean[][] newChoiceArray = new Boolean[3][3];

        temp[0][0] = state.marks[2][0];
        temp[0][1] = state.marks[1][0];
        temp[0][2] = state.marks[0][0];
        temp[1][0] = state.marks[2][1];

        temp[1][1] = state.marks[1][1];

        temp[1][2] = state.marks[0][1];
        temp[2][0] = state.marks[2][2];
        temp[2][1] = state.marks[1][2];
        temp[2][0] = state.marks[0][2];

        for(int r = 0; newChoiceArray.length > r; ++r) {
            for(int c = 0; newChoiceArray[r].length > c; ++c) {
                newChoiceArray[r][c] = state.choices[r][c].booleanValue();
            }
        }

        return new State(temp, newChoiceArray, state.degreesRelation + 90,
                         state.moveNum);
    }

    private State mirror() {
        Integer[][] temp = new Integer[3][3];
        Boolean[][] newChoiceArray = new Boolean[3][3];

        temp[0][0] = state.marks[0][2];
        temp[1][0] = state.marks[1][2];
        temp[2][0] = state.marks[2][2];

        temp[0][2] = state.marks[0][0];
        temp[1][2] = state.marks[1][0];
        temp[2][2] = state.marks[2][0];

        for(int r = 0; newChoiceArray.length > r; ++r) {
            for(int c = 0; newChoiceArray[r].length > c; ++c) {
                newChoiceArray[r][c] = state.choices[r][c].booleanValue();
            }
        }

        return new State(temp, newChoiceArray);
    }

    private Board(State state, ArrayList<State> derivStates){
        this.state = state;
        this.derivStates = derivStates;
    }

    public Board(State state) {
        this.state = state;

        generateDerivStates();
    }

    public Board() {
        this(new State());
    }

    public void generateDerivStates() {
        if(derivStates == null) {
            derivStates = new ArrayList<State>();
        }

        derivStates.clear();

        State ninetyState = ninetyDegreeRotate(state);
        State oneEightyState = ninetyDegreeRotate(ninetyState);
        State twoSeventyState = ninetyDegreeRotate(oneEightyState);
        State mirrorState = mirror();

        if(!equals((Object)ninetyState)) {
            derivStates.add(ninetyState);
        }

        if(!equals((Object)oneEightyState)) {
            derivStates.add(oneEightyState);
        }

        if(!equals((Object)twoSeventyState)) {
            derivStates.add(twoSeventyState);
        }

        if(!equals((Object)mirrorState)) {
            derivStates.add(mirrorState);
        }
    }

    public static boolean isCorner(int r, int c) {
        return (r == 0 && c == 0) || (r == 0 && c == 2) ||
               (r == 2 && c == 0) || (r == 2 && c == 2);
    }

    public static boolean arrayEquals(Integer[][] array, Integer[][] array2) {
        for(int r = 0; 3 > r; ++r) {
            for(int c = 0; 3 > c; ++c) {
                if(array[r][c] == null) {
                    return array2[r][c] == null;
                }

                if(array2[r][c] == null) {
                    return array[r][c] == null;
                }

                if(!array[r][c].equals(array2[r][c])) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean equals(Object o) {
        State state = null;

        if(o instanceof Board){
            state = ((Board) o).state;
        } else if(o instanceof State) {
            state = (State)o;
        }

        if(arrayEquals(this.state.marks, state.marks)) {
            return true;
        }

        for(State derivState : derivStates) {
            if(arrayEquals(derivState.marks, state.marks)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        return state.hashCode();
    }

    public void put(int r, int c, boolean x){
        for(int r2 = 0; 3 > r2; ++r2) {
            for(int c2 = 0; 3 > c2; ++c2) {
                state.choices[r2][c2] = !(r2 == r && c2 == c);
            }
        }

        ++state.moveNum;

        if(x) {
            state.marks[r][c] = 0;
        } else {
            state.marks[r][c] = 1;
        }
    }

    public void put(Board.State.Vector2 vector2, boolean x) {
        put(vector2.r, vector2.c, x);
    }

    @Override
    public Board clone() {
        ArrayList<State> newDerivStates = new ArrayList<>();

        for(State derivState : derivStates) {
            newDerivStates.add(derivState.clone());
        }

        return new Board(state.clone(), newDerivStates);
    }

    @Override
    public String toString(){
        return state.toString();
    }
}
