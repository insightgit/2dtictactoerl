// Bobby Youstra
// 2dTicTacToeRL

public class Main {
    public static void main(String[] args) {
        // by default, the AI will go first as X

        boolean aiIsX;
        TicTacToe ticTacToe = new TicTacToe();

        if(args.length > 0) {
            aiIsX = !args[0].toUpperCase().equals("-O");
        } else {
            aiIsX = true;
        }

        if(ticTacToe.training()){
            System.out.println("Training successful. Starting user game...");
            ticTacToe.play(aiIsX);
        } else {
            System.out.println("Something went wrong during training...");
        }
    }
}
