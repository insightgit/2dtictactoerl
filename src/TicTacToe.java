// Bobby Youstra
// 2dTicTacToeRL

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class TicTacToe {
    Board problemBoardX;
    Board.State.Vector2 associatedMoveX;
    Board problemBoardO;
    Board.State.Vector2 associatedMoveO;


    // I used a HashMap instead of an ArrayList or a HashSet because
    // an ArrayList wouldn't have good search performance and not preventing duplicates(linear),
    // and although a HashSet might be a little better due to preventing duplicates,
    // it still has the problem of not a good search performance (it only provided an iterator())
    // function as opposed to a proper performant get() function that HashMap provides.
    HashMap<Board.State, Board.State> constructedBoards; // X's constructed boards
    HashMap<Board.State, Board.State> opponentConstructedBoards; // O's constructed boards

    private Board.State.Vector2 getAIMove(Board.State state, boolean aiIsX) {
        ArrayList<Board.State.Vector2> availableMoves;
        Board.State originChoiceState = null;

        if(aiIsX) {
            originChoiceState = constructedBoards.get(state);
        } else {
            originChoiceState = opponentConstructedBoards.get(state);
        }

        if (originChoiceState != null) {
            state.choices = originChoiceState.adaptBasedOffDegreesRelation();
        }

        availableMoves = state.availableMoves();

        if (availableMoves.size() <= 0) {
            return null;
        } else {
            return availableMoves.get((int)(Math.random() * availableMoves.size()));
        }
    }

    public void negateMove(boolean isX) {
        // "eats the skittle" of the move associated with the problem board
        // and the derivative boards.

        if(isX) {
            if(problemBoardX.state.getMoveNum() <= 2 || problemBoardX.state.availableMoves().size() <= 1) {
                return;
            }

            if (constructedBoards.containsKey(problemBoardX.state)) {
                problemBoardX.state.choices = constructedBoards.
                        get(problemBoardX.state).
                        adaptBasedOffDegreesRelation();
            } else {
                problemBoardX.state.choices = problemBoardX.state.
                        adaptBasedOffDegreesRelation();
            }

            problemBoardX.state.choices[associatedMoveX.getRow()][associatedMoveX.getColumn()] = false;

            problemBoardX.generateDerivStates();

            constructedBoards.put(problemBoardX.state, problemBoardX.state);

            for (Board.State derivState : problemBoardX.derivStates) {
                constructedBoards.put(derivState, derivState);
            }
        } else {
            if(problemBoardO.state.getMoveNum() <= 3) {
                return;
            }

            if (opponentConstructedBoards.containsKey(problemBoardO.state)) {
                problemBoardO.state.choices = opponentConstructedBoards.
                        get(problemBoardO.state).
                        adaptBasedOffDegreesRelation();
            }

            problemBoardO.state.choices[associatedMoveO.getRow()][associatedMoveO.getColumn()] = false;

            problemBoardO.generateDerivStates();

            opponentConstructedBoards.put(problemBoardO.state, problemBoardO.state);

            for(Board.State derivState : problemBoardO.derivStates) {
                opponentConstructedBoards.put(derivState, derivState);
            }
        }
    }

    public boolean training() {
        // Open box
        // keep choose moves at random
        // when lost, go to first problem move
        // remove skittle
        // repeat

        Board board = new Board();
        boolean xTurn = true;
        int currentMatchNum = 0;

        constructedBoards = new HashMap<>();
        opponentConstructedBoards = new HashMap<>();

        while (1000000 > currentMatchNum) {
            int whoWon = board.state.whoWon();

            if (whoWon >= 0) {
                if (whoWon == 0) {
                    // X win
                    //System.out.println("X WIN");
                    //System.out.println(board.state);

                    negateMove(false);
                } else if (whoWon == 1) {
                    // O win
                    //System.out.println("O WIN");
                    //System.out.println(board.state);

                    negateMove(true);
                } else if(whoWon == 2){
                    //System.out.println("tie");
                    //System.out.println(board.state);

                    negateMove(xTurn);
                } else {
                    System.out.println("um this wasn't supposed to happen." +
                            " whoWon = " + whoWon);
                    return false;
                }

                ++currentMatchNum;
                board = new Board();
                problemBoardX = null;
                associatedMoveX = null;
                problemBoardO = null;
                associatedMoveO = null;

                xTurn = true;
                continue;
            } else {
                ArrayList<Board.State.Vector2> availableMoves;
                Board.State originChoiceState = null;

                if (xTurn && constructedBoards.containsKey(board.state)) {
                    originChoiceState = constructedBoards.get(board.state);
                } else if (opponentConstructedBoards.containsKey(board.state)) {
                    originChoiceState = opponentConstructedBoards.get(board.state);
                }

                if (originChoiceState != null) {
                    board.state.choices = originChoiceState.adaptBasedOffDegreesRelation();
                }

                availableMoves = board.state.availableMoves();

                if (availableMoves.size() <= 0) {
                    //System.out.println("forfeit on xTurn=" + xTurn);
                    //System.out.println(board.state);

                    negateMove(xTurn);

                    board = new Board();
                    problemBoardX = null;
                    associatedMoveX = null;
                    problemBoardO = null;
                    associatedMoveO = null;
                    ++currentMatchNum;

                    xTurn = true;
                    continue;
                } else {
                    Board.State.Vector2 move = availableMoves.get((int)(Math.random() * availableMoves.size()));

                    if (xTurn) {
                        problemBoardX = board.clone();
                        associatedMoveX = move;
                    } else {
                        problemBoardO = board.clone();
                        associatedMoveO = move;
                    }

                    board.put(move.getRow(), move.getColumn(), xTurn);
                }


            }

            xTurn = !xTurn;
        }

        return true;
    }

    private Board.State.Vector2 getUserMove(Board.State state) {
        Scanner s = new Scanner(System.in);
        String move;
        Board.State.Vector2 moveVector;
        String[] coords;

        while(true) {
            System.out.println("Your move:");
            move = s.nextLine();

            coords = move.split(",");

            moveVector = new Board.State.Vector2(Integer.parseInt(coords[0]),
                                                 Integer.parseInt(coords[1]));

            if(state.isOccupied(moveVector)) {
                System.out.println("Invalid move!");
            } else {
                return moveVector;
            }
        }
    }

    public void play(boolean aiIsX) {
        Board board = new Board();
        boolean xTurn = true;

        while(board.state.whoWon() == -1) {
            Board.State.Vector2 aiMove;

            if(xTurn) {
                if(aiIsX) {
                    aiMove = getAIMove(board.state, aiIsX);

                    if(aiMove == null) {
                        System.out.println("AI forfeits.");
                        return;
                    } else {
                        System.out.println("AI move: " + aiMove);
                        board.put(aiMove, aiIsX);
                    }
                } else {
                    board.put(getUserMove(board.state), !aiIsX);
                }
            } else {
                if(aiIsX) {
                    board.put(getUserMove(board.state), !aiIsX);
                } else {
                    aiMove = getAIMove(board.state, aiIsX);

                    if(aiMove == null) {
                        System.out.println("AI forfeits.");
                        return;
                    } else {
                        System.out.println("AI move: " + aiMove);
                        board.put(aiMove, aiIsX);
                    }
                }
            }

            System.out.println(board + "\n");
            xTurn = !xTurn;
        }

        int whoWon = board.state.whoWon();

        switch(whoWon) {
            case 0:
                System.out.println("X won!");
                break;
            case 1:
                System.out.println("O won!");
                break;
            case 2:
                System.out.println("Tie!");
                break;
            default:
                System.out.println("Something went wrong... whoWon=" + whoWon);
                break;
        }
    }
}
